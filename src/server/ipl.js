const fs = require('fs');
const Papa = require('papaparse');
const matchesCSV = fs.readFileSync('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/data/matches.csv', 'utf8');
const deliveriesCSV = fs.readFileSync('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/data/deliveries.csv', 'utf8');

const deliveriesJSON = Papa.parse(deliveriesCSV, {
    header: true
});
const deliveries = deliveriesJSON['data'];

const matchJSON = Papa.parse(matchesCSV, {
    header: true
});
const matches = matchJSON['data'];

//Number of matches played per year for all the years in IPL.

const matchesPerYear = function (data) {
    let object = data.reduce((accumulator, current) => {
        if (current.id !== '') {
            if (accumulator[current.season]) {
                accumulator[current.season]++;
            } else {
                accumulator[current.season] = 1
            }
            return accumulator;
        } else {
            return accumulator;
        }
    }, {})
    return object;
}

const yearWise = matchesPerYear(matches);

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/matchesPerYear.json', JSON.stringify(yearWise), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Matches per year JSON File Created Succesfully");
    }
});

// Number of matches won per team per year in IPL.

const matchesWon = function (data) {
    let object = data.reduce((accumulator, current) => {
        if (current.id !== '') {
            if (accumulator.hasOwnProperty(current.winner)) {
                if (accumulator[current.winner].hasOwnProperty(current.season)) {
                    accumulator[current.winner][current.season] = accumulator[current.winner][current.season] + 1;
                } else {
                    accumulator[current.winner][current.season] = 1;
                }
            } else {
                accumulator[current.winner] = {};
                accumulator[current.winner][current.season] = 1;
            }
            return accumulator;
        } else {
            return accumulator;
        }
    }, {});
    return object;
}

const matchesWonPerYear = matchesWon(matches);

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/matchesWonPerYearPerTeam.json', JSON.stringify(matchesWonPerYear), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Matches won per year per team JSON File Created Succesfully");
    }
});

// Extra runs conceded per team in the year 2016

const extraRunsIn2016 = function (matches, deliveries) {

    const matchID = function (matches) {
        let matchID = matches.reduce((accumulator, current) => {
            if (current.season == 2016) {
                return [...accumulator, parseInt(current['id'])];
            }
            return accumulator;
        }, [])
        return matchID;
    }

    const matchId2016 = matchID(matches);

    let deliveryData2016 = deliveries.reduce((accumulator, current) => {
        if (matchId2016.includes(parseInt(current.match_id))) {
            accumulator.push(current);
        }
        return accumulator;
    }, []);

    let extraRuns = deliveryData2016.reduce((accumulator, current) => {
        if (matchId2016.includes(parseInt(current.match_id))) {
            if (accumulator.hasOwnProperty(current.bowling_team)) {
                accumulator[current.bowling_team] += parseInt(current.extra_runs);
            } else {
                accumulator[current.bowling_team] = parseInt(current.extra_runs);
            }
        }
        return accumulator;
    }, {});

    return extraRuns;
}

const extraRunsPerTeam = extraRunsIn2016(matches, deliveries);

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/extraRunsPerTeam.json', JSON.stringify(extraRunsPerTeam), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Extra runs per team JSON File Created Succesfully");
    }
});

// Top 10 economical bowlers in the year 2015

const topEconomyBowler = function (matches, deliveries) {

    const matchID = function (matches) {
        let matchID = matches.reduce((accumulator, current) => {
            if (current.season == 2015) {
                return [...accumulator, parseInt(current['id'])];
            }
            return accumulator;
        }, [])
        return matchID;
    }

    const matchId2015 = matchID(matches);

    let deliveryData2015 = deliveries.reduce((accumulator, current) => {
        if (matchId2015.includes(parseInt(current.match_id))) {
            accumulator.push(current);
        }
        return accumulator;
    }, []);

    let economy = deliveryData2015.reduce((accumulator, currentValue) => {
        if (matchId2015.includes(parseInt(currentValue.match_id))) {
            if (accumulator[currentValue.bowler]) {
                accumulator[currentValue.bowler]['runs_conceded'] += parseInt(currentValue.total_runs);
                accumulator[currentValue.bowler]['total_balls']++;
                accumulator[currentValue.bowler]['economy'] = (accumulator[currentValue.bowler]['runs_conceded'] / ((accumulator[currentValue.bowler]['total_balls']) / 6)).toFixed(2);
            } else {
                accumulator[currentValue.bowler] = {};
                accumulator[currentValue.bowler]['runs_conceded'] = parseInt(currentValue.total_runs);
                accumulator[currentValue.bowler]['total_balls'] = 1;
            }
        }
        return accumulator;
    }, {});

    return Object.fromEntries(Object.entries(economy).sort((a, b) => a[1].economy - b[1].economy).slice(0, 10));

}

const bestBowlers = topEconomyBowler(matches, deliveries);

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/topTenBowlersByEconomy.json', JSON.stringify(bestBowlers), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Top 10 economical bowlers in the year 2015 JSON File Created Succesfully");
    }
});

// number of times each team won the toss and also won the match

const tossAndWinner = function(matches) {
    let winnersArray = matches.reduce((accumulator, current) => {
        if(!current.id) {
            return accumulator;
        } else {
            if (current.winner === current.toss_winner) {
                if (accumulator[current.winner]) {
                    accumulator[current.winner]++;
                } else {
                    accumulator[current.winner] = 1;
                }
            }
        }
        return accumulator;
    }, {});
    return winnersArray;
}

const winningTeam = tossAndWinner(matches);

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/tossWinnersAndMatchWinners.json', JSON.stringify(winningTeam), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Number of times each team won the toss and also won the match JSON File Created Succesfully");
    }
});

// players who has won the highest number of Player of the Match awards for each season

const playersOfTheSeason = function(matches) {

    let manOfMatch = matches.reduce((accumulator, current) => {
        if(current.id !== '') {
            if(accumulator[current.season]) {
                if(accumulator[current.season].hasOwnProperty(current.player_of_match)) {
                    accumulator[current.season][current.player_of_match]++;
                } else {
                    
                    accumulator[current.season][current.player_of_match] =1
                }
            } else {
                accumulator[current.season] = {}
            }
        }
        return accumulator;
    }, {});

    let result = Object.entries(manOfMatch).reduce((accumulator, current) => {
        let topPlayer = Object.entries(current[1]).sort((firstValue,secondValue) => {
             return (secondValue[1]-firstValue[1]);
        }).slice(0, 1);
        accumulator[current[0]] = Object.fromEntries(topPlayer);
        return accumulator;
    },{});

    return result;
}

const topPlayersEachSeason = playersOfTheSeason(matches);

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/topPlayersEachSeason.json', JSON.stringify(topPlayersEachSeason), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Players who won the highest number of Player of the Match awards for each season JSON File Created Succesfully");
    }
});

// Top economical bowler in super overs

const findBestBowlerInSuperover = function(deliveries) {

    let bowlerData = deliveries.reduce((accumulator, current) => {
        if (current.match_id != '') {
            if (current.is_super_over != 0) {
                if (accumulator[current.bowler]) {
                    accumulator[current.bowler]['runs'] += parseInt(current.total_runs);
                    accumulator[current.bowler]['balls']++;
                    accumulator[current.bowler]['economy'] = parseFloat((accumulator[current.bowler]['runs'] / ((accumulator[current.bowler]['balls']) / 6)).toFixed(2));
                } else {
                    accumulator[current.bowler] = {};
                    accumulator[current.bowler]['runs'] = parseInt(current.total_runs);
                    accumulator[current.bowler]['balls'] = 1;
                }
    
            }
        }
        return accumulator;
    }, {});
    
    let topBowler = Object.fromEntries(Object.entries(bowlerData).sort((first, second) => {
        if (first[1].economy > second[1].economy) {
            return 1;
        } else {
            return -1;
        }
    }).slice(0, 1));

    return topBowler;
}

const bestBowler = findBestBowlerInSuperover(deliveries);

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/bestBowlerInSuperovers.json', JSON.stringify(bestBowler), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Best bowler in super overs JSON File Created Succesfully");
    }
});

// Strike rate of a batsman for all the years

const findYearWiseStrikeRate = function (year, player, matches, deliveries) {

    const matchID = function (matches) {
        let matchID = matches.map((match) => {
            if (parseInt(match.season) === year) {
                return (parseInt(match.id));
            }
        });
        return matchID;
    }

    const matchIdData = matchID(matches);

    let batsmanData = deliveries.reduce((accumulator, current) => {
        if (matchIdData.includes(parseInt(current.match_id))) {
            if (current.batsman === player) {
                if (accumulator.hasOwnProperty(current.batsman)) {
                    accumulator[current.batsman]['runs'] += parseInt(current.batsman_runs);
                    accumulator[current.batsman]['balls']++;
                    accumulator[current.batsman]['strike_rate'] = parseFloat(((accumulator[current.batsman]['runs'] / accumulator[current.batsman]['balls']) * 100).toFixed(3));
                } else {
                    accumulator[current.batsman] = {};
                    accumulator[current.batsman]['year'] = year;
                    accumulator[current.batsman]['runs'] = parseInt(current.batsman_runs);
                    accumulator[current.batsman]['balls'] = 1;
                }
            }
        }
        return accumulator;
    }, {});

    return (batsmanData);

}

let yearData = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017];
let player = 'V Kohli';

let playerStrikeRate = yearData.map((year) => {
    return (findYearWiseStrikeRate(year, player, matches, deliveries));
});

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/playerStrikeRateSeasonWise.json', JSON.stringify(playerStrikeRate), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Strike of a batsman for all the seasons JSON File Created Succesfully");
    }
});

// highest number of times one player has been dismissed by another player

const findDismissals = function(deliveries) {

    let mostDismissedPlayer = {batsman : null, bowler : null, dismissals : 0};

    deliveries.filter((ball) => {
        if (ball.dismissal_kind !== '' && ball.dismissal_kind !== 'run out') {
            return true;
        }
    })
    .reduce((accumulator, current) => {

        if (accumulator[current.batsman] === undefined) {
            accumulator[current.batsman] = {}
        }

        if (accumulator[current.batsman][current.bowler] === undefined) {
            accumulator[current.batsman][current.bowler] = 0;
        }

        accumulator[current.batsman][current.bowler]++;

        if (mostDismissedPlayer.dismissals < accumulator[current.batsman][current.bowler]) {
            mostDismissedPlayer.batsman = current.batsman;
            mostDismissedPlayer.bowler = current.bowler;
            mostDismissedPlayer.dismissals = accumulator[current.batsman][current.bowler];
        }

        return accumulator;
        
    }, {});

    return mostDismissedPlayer;

}

let playerDismissedByAnotherPlayer = findDismissals(deliveries);

fs.writeFile('/home/sharun/Documents/MountBlue Projects/IPL Data Project 1/src/public/output/playerDismissedByAnotherPlayerHighest.json', JSON.stringify(playerDismissedByAnotherPlayer), (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log("Most number of dismissals of one player by another JSON File Created Succesfully");
    }
});

